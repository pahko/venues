Foursquare Venues Like API
--------------------------

Dependencies:
"""""""""""""

* `postgis <https://docs.djangoproject.com/en/1.8/ref/contrib/gis/install/postgis/>`_
* python 2.7.X
* virtualenv

Getting Started
"""""""""""""""

On local environment:

.. code-block:: bash

    $ cd <path-to-project>/
    $ pip install -r dev_requirements.txt
    $ cd venues # django project
    $ ./manage.py migrate
    $ ./manage.py runserver

go to http://localhost:8000/v1

Runnig Tests
""""""""""""

.. code-block:: bash

    $ py.test


# Run docs

.. code-block:: bash

    $ mkdocs  serve  -a 0.0.0.0:8001

go to http://localhost:8001
