# Venue API

Venues API V1

### Authentication:

Venues uses JWT to authenticate calls to API.

to create an user make a `POST` to `/accounts/register/` with `username`,
`email` and `password`, it will return a `token` that can be used to query the API
as an authenticated user using the headers: `Authorization: JWT <token>`.

By default, the token expires in 10 days, to get a new token make a `POST` call to
`/api-token-auth/` with `username` and `password`.


### Accounts

#### POST /accounts/register/

<table>
    <tr>
        <td>
            <strong>username</strong>
        </td>
        <td>
            username to be used at login. <br><br>
            <strong>Example of values:</strong> pahko
        </td>
    </tr>

    <tr>
        <td>
            <strong>email</strong> <br>
        </td>
        <td>
            User email adress.<br><br>

            <strong>Example of values:</strong> test@gmail.com
        </td>
    </tr>
    <tr>
        <td>
            <strong>password</strong> <br>
        </td>
        <td>
            Password to use at login.<br><br>

            <strong>Example of values:</strong> secret123
        </td>
    </tr>
</table>


```json
{
    "username": "user",
    "email":"test@gmail.xd",
    "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."
}
```


### Venues

#### GET /venues/

Returns a list of venues filtered by address and distance.

Either an `address` must be provided in order to filter venues by location.
An optional `distance` parameter could be provided otherwise the default distance is `10` miles.

<table>
    <tr>
        <td>
            <strong>address</strong>
        </td>
        <td>
            Location of the venue. <br><br>
            <strong>Example of values:</strong> 200 5th Avenue 10010
        </td>
    </tr>

    <tr>
        <td>
            <strong>distance</strong> <br>
            (optional)
        </td>
        <td>
            Distance from current address or location to show Venue.<br><br>
            <strong>default:</strong> 10 <br>

            <strong>Example of values:</strong> 200 5th Avenue 10010
        </td>
    </tr>
</table>


```json
[{
    "address": "200 5th Avenue",
    "city": "New York City",
    "id": 1,
    "lat": null,
    "lng": null,
    "name": "Shake Shack",
    state: "NY",
    zip_code: "10010"
}..]
```


#### POST /venues/

Creates a Venue. If not `lat` or `lng` are specified, will try to retrieve those values
using Google Maps Geo API based on address provided.

<table>
    <tr>
        <td>
            <strong>name</strong> <br>
        </td>
        <td>
            Name of the venue. <br><br>
            <strong>Example of values:</strong> Shake Shack
        </td>
    </tr>

    <tr>
        <td>
            <strong>address</strong> <br>
            (optional)
        </td>
        <td>
            Location of the venue. <br><br>
            <strong>Example of values:</strong> Madison Square Park
        </td>
    </tr>

    <tr>
        <td>
            <strong>lng</strong> <br>
            (optional)
        </td>
        <td>
            Latitude where the venue is located. <br><br>
            <strong>Example of values:</strong> 40.7414492
        </td>
    </tr>

    <tr>
        <td>
            <strong>lon</strong> <br>
            (optional)
        </td>
        <td>
            Loongitude where the venue is located. <br><br>
            <strong>Example of values:</strong> -73.9903933
        </td>
    </tr>

    <tr>
        <td>
            <strong>city</strong> <br>
            (optional)
        </td>
        <td>
            City where the venue is located. <br><br>
            <strong>Example of values:</strong> New York
        </td>
    </tr>

    <tr>
        <td>
            <strong>state</strong> <br>
            (optional)
        </td>
        <td>
            State abbreviation where the venue is located. <br><br>
            <strong>Example of values:</strong> NY
        </td>
    </tr>

    <tr>
        <td>
            <strong>zip_code</strong> <br>
            (optional)
        </td>
        <td>
            Zip Code where the venue is located. <br><br>
            <strong>Example of values:</strong> 10010
        </td>
    </tr>
</table>

### Reviews

#### GET  /venues/{venue_id}/reviews/

Get a list of reviews for a given venue ordered by date of review from newest to oldest.

```json
[{
    "id": 30
    "review_date": "2015-10-26T23:31:44.233002Z"
    "score": 2
    "text": "Amazing!!"
    writer: 12
}..]
```

#### POST  /venues/{venue_id}/reviews/

Creates a review for Venue with `venue_id`. Defualt writer is the user who is making the call.

<table>
    <tr>
        <td>
            <strong>text</strong> <br>
        </td>
        <td>
            Review text. <br><br>
            <strong>Example of values:</strong> This place has an amazing view.
        </td>
    </tr>

    <tr>
        <td>
            <strong>score</strong> <br>
            (optional)
        </td>
        <td>
            Score from 0 to 5. <br><br>
            <strong>default:</strong> 0 <br>
            <strong>Example of values:</strong> 5
        </td>
    </tr>
</table>


### Comments

#### GET  /venues/{venue_id}/reviews/{review_id}/comments/

Get a list of comments for a given venue with `venue_id` and review with `review_id`
ordered by date of comment from newest to oldest.

```json
[{
    "id": 30
    "comment_date": "2015-10-26T23:31:44.233002Z"
    "text": "what did you just say ???"
    writer: 13
}..]
```

#### POST  /venues/{venue_id}/reviews/{review_id}/comments/

Creates a comment for Venue with `venue_id` and Review with `review_id`.
Defualt writer is the user who is making the call.

<table>
    <tr>
        <td>
            <strong>text</strong> <br>
        </td>
        <td>
            Comment text. <br><br>
            <strong>Example of values:</strong> This guy knows something.
        </td>
    </tr>
</table>
