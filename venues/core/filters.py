from django.conf import settings
from django.contrib.gis.geos import Point, fromstr
from django.contrib.gis.measure import D

from rest_framework import filters
import django_filters

from core.models import Venue
from core import geo


class VenuesFilterset(filters.FilterSet):
    address = django_filters.MethodFilter(action='filter_by_address')

    class Meta:
        model = Venue
        fields = ('address', )

    def filter_by_address(self, queryset, value):
        lat, lng = geo.get_coordinates(value)
        if lat and lng:
            point = Point(lat, lng)
            distance = self.data.get('distance', settings.DEFAULT_DISTANCE_LIST)
            point_str = fromstr(str(point), srid=settings.GEO_DAFAULT_SRIM)
            return Venue.objects.filter(point__distance_lte=(point_str, D(mi=distance)))

        return queryset.model.objects.none()
