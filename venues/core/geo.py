import urllib
import logging

from django.conf import settings

import requests


logger = logging.getLogger(__name__)


def get_coordinates(address):
    """
    Returns latitute and longitude for a given address using google
    maps api
    """
    params = {
        'sensor': False,
        'address': urllib.quote_plus(address)
    }
    url = '{}/geocode/json'.format(settings.GOOGLE_MAPS_API_URL)

    try:
        json_response = requests.get(url, params=params).json()
    except ValueError:
        logger.warning('Invalid json response from G MAPS API object {}'.format(params['address']))
        return None, None

    lat = json_response['results'][0]['geometry']['location']['lat']
    lng = json_response['results'][0]['geometry']['location']['lng']
    return lat, lng
