from rest_framework.response import Response


class ListPaginationMixin(object):
    """
    Helper to handle pagination for overriden `list` method
    """
    def paginate_queryset_view(self, queryset):
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
