import logging

from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    UserManager
)
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.core import validators
from django.core.mail import send_mail
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from localflavor.us.models import USStateField, USZipCodeField
from rest_framework_jwt.settings import api_settings

from core import geo


logger = logging.getLogger(__name__)


class Review(models.Model):
    writer = models.ForeignKey('CoreUser')
    text = models.TextField()
    score = models.IntegerField(
        default=0,
        validators=[
            validators.MinValueValidator(0, message='Enter a value bigger than 0'),
            validators.MinValueValidator(5, message='Enter a value smaller than 6')
        ]
    )
    review_date = models.DateTimeField(auto_now_add=True)
    comments = models.ManyToManyField('Comment', blank=True)

    class Meta:
        ordering = ['-review_date']


@python_2_unicode_compatible
class Venue(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = USStateField(blank=True)
    zip_code = USZipCodeField(blank=True)
    lng = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    point = models.PointField(srid=settings.GEO_DAFAULT_SRIM, blank=True, null=True)
    reviews = models.ManyToManyField('Review', blank=True)
    likes = models.ManyToManyField('Vote', blank=True)
    checkins = models.ManyToManyField('CoreUser', through='UserVenueCheckin')

    objects = models.GeoManager()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self._save_point()
        super(Venue, self).save(*args, **kwargs)

    def _save_point(self):
        """
        If lat and lng are None, try to reverse geocode using address fields
        """
        if not (self.lat or self.lng):
            lat, lng = geo.get_coordinates(self.get_full_address())
            self.lat = lat
            self.lng = lng
            self.point = Point(self.lat, self.lng)

        if self.lat and self.lng and not self.point:
            self.point = Point(self.lat, self.lng)

    def get_full_address(self):
        return '{address} {city} {state} {zip_code}'.format(
            address=self.address,
            city=self.city,
            state=self.state,
            zip_code=self.zip_code
        )


class UserVenueCheckin(models.Model):
    checkin_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('CoreUser')
    venue = models.ForeignKey('Venue')


class Vote(models.Model):
    liked = models.BooleanField(default=True)
    user = models.ForeignKey('CoreUser')


class CoreUser(AbstractBaseUser, PermissionsMixin):
    """Venue User custom Model"""
    username = models.CharField(
        _('username'), max_length=30, unique=True,
        help_text=_('Required. 30 characters or fewer. Letters, digits and '
                    '@/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(r'^[\w.@+-]+$',
                                      _('Enter a valid username. '
                                        'This value may contain only letters, numbers '
                                        'and @/./+/-/_ characters.'), 'invalid'),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        })
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(
        _('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    visited_venues = models.ForeignKey('Venue', blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_token(self):
        """
        Gets a JWT Token to this User
        """
        if self.pk:
            payload = api_settings.JWT_PAYLOAD_HANDLER(self)
            token = api_settings.JWT_ENCODE_HANDLER(payload)
            return token


class Comment(models.Model):
    comment_date = models.DateTimeField(auto_now_add=True)
    writer = models.ForeignKey('CoreUser')
    text = models.TextField()

    class Meta:
        ordering = ['-comment_date']
