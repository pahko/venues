from rest_framework import serializers

from core.models import CoreUser, Venue, UserVenueCheckin, Review, Comment


class RegisterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreUser
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

    def create_user(self):
        self.object = self.Meta.model.objects.create_user(**self.validated_data)

    @property
    def data(self):
        data = super(RegisterUserSerializer, self).data.copy()
        del data['password']
        data['token'] = self.object.get_token()
        return data


class VenueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venue
        fields = (
            'id', 'name', 'address', 'city', 'state', 'zip_code', 'lat',
            'lng'
        )


class UserVenueCheckinSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserVenueCheckin


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'text', 'writer', 'score', 'review_date')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'text', 'writer', 'comment_date')
