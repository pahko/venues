from django.conf.urls import url, include

from rest_framework_nested import routers
from core import views


router = routers.SimpleRouter()

router.register(r'accounts', views.UserAccountViewSet, base_name='accounts')
router.register(r'venues', views.VenueViewSet, base_name='venues')

venues_router = routers.NestedSimpleRouter(router, r'venues', lookup='venue')
venues_router.register(r'reviews', views.ReviewViewSet, base_name='venue-reviews')

reviews_router = routers.NestedSimpleRouter(venues_router, r'reviews', lookup='review')
reviews_router.register(r'comments', views.CommentViewSet, base_name='review-comments')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(venues_router.urls)),
    url(r'^', include(reviews_router.urls)),
]
