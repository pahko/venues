from django.utils import timezone

from rest_framework import status, viewsets, mixins, filters
from rest_framework.decorators import list_route, detail_route
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from core.filters import VenuesFilterset
from core.mixins import ListPaginationMixin
from core.models import Venue, UserVenueCheckin, Review, Comment, Vote
from core.serializers import (
    CommentSerializer,
    RegisterUserSerializer,
    ReviewSerializer,
    UserVenueCheckinSerializer,
    VenueSerializer,
)


class UserAccountViewSet(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    @list_route(methods=['POST'])
    def register(self, request, *args, **kwargs):
        """
        Creates an CoreUser Account.
        """
        serializer = RegisterUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create_user()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class VenueViewSet(viewsets.ModelViewSet):
    queryset = Venue.objects.all()
    serializer_class = VenueSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = VenuesFilterset

    @detail_route(methods=['POST'])
    def checkin(self, request, *args, **kwargs):
        """
        Records a user visit to a Venue, one visit per day is allowed.
        """
        venue = self.get_object()

        try:
            now = timezone.now()
            UserVenueCheckin.objects.get(
                user=request.user,
                venue=venue,
                checkin_date__year=now.year,
                checkin_date__month=now.month,
                checkin_date__day=now.day,
            )
            raise ValidationError({'error': 'You already made a checkin in this Venue today.'})
        except UserVenueCheckin.DoesNotExist:
            pass

        user_venue_checkin = UserVenueCheckin.objects.create(user=request.user, venue=venue)
        serializer = UserVenueCheckinSerializer(instance=user_venue_checkin)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @detail_route(methods=['POST'])
    def vote(self, request, *args, **kwargs):
        """
        Thumbs up or thumbs down a venue.
        """
        venue = self.get_object()

        try:
            vote = Vote.objects.get(user=request.user)
            vote.liked = request.data.get('liked')
        except Vote.DoesNotExist:
            vote = Vote.objects.create(user=request.user, liked=request.data.get('liked'))

        venue.likes.add(vote)
        return Response({'success': True}, status=status.HTTP_201_CREATED)


class ReviewViewSet(ListPaginationMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('writer',)

    def create(self, request, venue_pk=None, *args, **kwargs):
        venue = get_object_or_404(Venue.objects.all(), pk=venue_pk)
        data = request.data
        data.update({'writer': request.user.pk})
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        venue.reviews.add(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, venue_pk=None, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().filter(venue__pk=venue_pk))
        return self.paginate_queryset_view(queryset=queryset)

    def retrieve(self, request, pk=None, venue_pk=None, *args, **kwargs):
        queryset = self.get_queryset().filter(venue__pk=venue_pk, pk=pk)
        instance = get_object_or_404(queryset, pk=pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class CommentViewSet(ListPaginationMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def create(self, request, venue_pk=None, review_pk=None, *args, **kwargs):
        review = get_object_or_404(Review.objects.all(), pk=review_pk, venue__pk=venue_pk)
        data = request.data
        data.update({'writer': request.user.pk})
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        review.comments.add(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, venue_pk=None, review_pk=None, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().filter(review__pk=review_pk, review__venue__pk=venue_pk))
        return self.paginate_queryset_view(queryset=queryset)

    def retrieve(self, request, pk=None, venue_pk=None, review_pk=None, *args, **kwargs):
        queryset = self.get_queryset().filter(review__pk=review_pk, review__venue__pk=venue_pk, pk=pk)
        instance = get_object_or_404(queryset, pk=pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
