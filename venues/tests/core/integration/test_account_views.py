from rest_framework.reverse import reverse
import pytest

from core.views import UserAccountViewSet


class TestVenueAPI(object):
    @pytest.mark.django_db
    def test_create_account(self, request_factory):
        view = UserAccountViewSet.as_view({'post': 'register'})
        url = reverse('accounts-register')

        data = {
            'username': 'testing',
            'email': 'testing@gmail.com',
            'password': 'haha',
        }
        request = request_factory.post(url, data=data)
        response = view(request)

        assert response.status_code == 201
        assert response.data['token']
        assert response.data['username'] == data['username']
