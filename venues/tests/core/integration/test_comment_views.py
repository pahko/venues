from rest_framework.reverse import reverse
from rest_framework.test import force_authenticate
import pytest

from core.views import CommentViewSet


@pytest.fixture
def dev_venue(model_mommy):
    return model_mommy.make(
        'Venue', name='Shake Shack', address='200 5th Avenue', city='New York City',
        state='NY', zip_code='10010', lat=40.7030941, lng=-73.9563683
    )


class TestCommentAPI(object):
    @pytest.mark.django_db
    def test_create_comment(self, request_factory, dev_user, dev_venue, model_mommy):
        review = model_mommy.make('Review', text='my_review', writer=dev_user)
        dev_venue.reviews.add(review)
        view = CommentViewSet.as_view({'post': 'create'})
        url = reverse('review-comments-list', kwargs={'venue_pk': dev_venue.pk, 'review_pk': review.pk})

        data = {'text': 'My Comment'}
        request = request_factory.post(url, data=data)
        force_authenticate(request, user=dev_user)
        response = view(request, venue_pk=dev_venue.pk, review_pk=review.pk)

        assert response.status_code == 201
        assert response.data['id']
        assert response.data['text'] == data['text']

    @pytest.mark.django_db
    def test_list_comments_by_recent_date(self, request_factory, dev_user, dev_venue, model_mommy):
        review = model_mommy.make('Review', text='my_review', writer=dev_user)
        dev_venue.reviews.add(review)
        comment_1 = model_mommy.make('Comment', writer=dev_user, text='comment 1')
        comment_2 = model_mommy.make('Comment', writer=dev_user, text='comment 2')
        review.comments.add(comment_1, comment_2)

        view = CommentViewSet.as_view({'get': 'list'})
        url = reverse('review-comments-list', kwargs={'venue_pk': dev_venue.pk, 'review_pk': review.pk})

        request = request_factory.get(url)
        force_authenticate(request, user=dev_user)
        response = view(request, venue_pk=dev_venue.pk, review_pk=review.pk)

        assert response.status_code == 200
        assert len(response.data) == 2
        assert response.data[0]['text'] == comment_2.text
        assert response.data[1]['text'] == comment_1.text
