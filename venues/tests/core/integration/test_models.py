import pytest

from core.models import Venue


class TestVenue(object):
    @pytest.mark.django_db
    def test_save_point_by_address(self, mock):
        values = {
            'address': '200 5th Avenue',
            'zip_code': 10010
        }

        request_mock_data = {
            'results': [{'geometry': {'location': {'lat': 40.7421643, 'lng': -73.9898936}}}]
        }
        get_mock = mock.MagicMock()
        get_mock.json.return_value = request_mock_data
        with mock.patch('core.geo.requests.get', return_value=get_mock):
            venue = Venue.objects.create(**values)

        assert venue.lat
        assert venue.lng
        assert venue.point
