from rest_framework.reverse import reverse
from rest_framework.test import force_authenticate
import pytest

from core.views import ReviewViewSet


@pytest.fixture
def dev_venue(model_mommy):
    return model_mommy.make(
        'Venue', name='Shake Shack', address='200 5th Avenue', city='New York City',
        state='NY', zip_code='10010', lat=40.7030941, lng=-73.9563683
    )


class TestReviewAPI(object):
    @pytest.mark.django_db
    def test_create_review(self, request_factory, dev_user, dev_venue):
        view = ReviewViewSet.as_view({'post': 'create'})
        url = reverse('venue-reviews-list', kwargs={'venue_pk': dev_venue.pk})

        data = {'text': 'My Review', 'score': 4}
        request = request_factory.post(url, data=data)
        force_authenticate(request, user=dev_user)
        response = view(request, venue_pk=dev_venue.pk)

        assert response.status_code == 201
        assert response.data['id']
        assert response.data['text'] == data['text']
        assert response.data['score'] == data['score']

    @pytest.mark.django_db
    def test_list_review_by_recent_date(self, request_factory, dev_user, dev_venue, model_mommy):
        view = ReviewViewSet.as_view({'get': 'list'})
        url = reverse('venue-reviews-list', kwargs={'venue_pk': dev_venue.pk})

        review_1 = model_mommy.make('Review', text='first', score=5)
        review_2 = model_mommy.make('Review', text='second', score=4)
        dev_venue.reviews.add(review_1, review_2)

        request = request_factory.get(url)
        force_authenticate(request, user=dev_user)
        response = view(request, venue_pk=dev_venue.pk)

        assert response.status_code == 200
        assert len(response.data) == 2
        assert response.data[0]['text'] == review_2.text
        assert response.data[1]['text'] == review_1.text
