from rest_framework.reverse import reverse
from rest_framework.test import force_authenticate
import pytest

from core.views import VenueViewSet


@pytest.fixture
def dev_venue(model_mommy):
    return model_mommy.make(
        'Venue', name='Shake Shack', address='200 5th Avenue', city='New York City',
        state='NY', zip_code='10010', lat=40.7030941, lng=-73.9563683
    )


class TestVenueAPI(object):
    @pytest.mark.django_db
    def test_create_venue(self, request_factory, dev_user):
        view = VenueViewSet.as_view({'post': 'create'})
        url = reverse('venues-list')

        data = {
            'name': 'Shake Shack',
            'address': '200 5th Avenue',
            'city': 'New York City',
            'state': 'NY',
            'zip_code': '10010',
            'lat': 40.7030941,
            'lng': -73.9563683
        }
        request = request_factory.post(url, data=data)
        force_authenticate(request, user=dev_user)
        response = view(request)

        assert response.status_code == 201
        assert response.data['id']
        assert response.data['name'] == data['name']
        assert response.data['lat'] == data['lat']
        assert response.data['lng'] == data['lng']

    @pytest.mark.django_db
    def test_list_venues_by_distance_and_address(self, request_factory, dev_user, model_mommy, dev_venue, mock):
        view = VenueViewSet.as_view({'get': 'list'})
        url = reverse('venues-list')
        near_venue = dev_venue
        model_mommy.make(
            'Venue', name='In and Out Burger', address='7009 W Sunset Boulevard', city='Los Angeles',
            state='LA', zip_code='90028', lat=34.0981971, lng=-118.4117471
        )

        params = {'distance': 100, 'address': 'Union Square W #1, New York, NY 10003'}
        request = request_factory.get(url, params)
        force_authenticate(request, user=dev_user)
        request_mock_data = {
            'results': [{'geometry': {'location': {'lat': 40.7325995, 'lng': -73.985007}}}]
        }
        get_mock = mock.MagicMock()
        get_mock.json.return_value = request_mock_data
        with mock.patch('core.geo.requests.get', return_value=get_mock):
            response = view(request)

        assert response.status_code == 200
        assert len(response.data) == 1
        assert response.data[0]['name'] == near_venue.name

    @pytest.mark.django_db
    def test_checkin(self, request_factory, dev_user, dev_venue):
        view = VenueViewSet.as_view({'post': 'checkin'})
        url = reverse('venues-checkin', kwargs={'pk': dev_venue.pk})

        request = request_factory.post(url)
        force_authenticate(request, user=dev_user)
        response = view(request, pk=dev_venue.pk)

        assert response.status_code == 201
        assert response.data['user'] == dev_user.pk
        assert response.data['venue'] == dev_venue.pk

    @pytest.mark.django_db
    def test_checkin_twice(self, request_factory, dev_user, dev_venue, model_mommy):
        view = VenueViewSet.as_view({'post': 'checkin'})
        url = reverse('venues-checkin', kwargs={'pk': dev_venue.pk})
        model_mommy.make('UserVenueCheckin', user=dev_user, venue=dev_venue)

        request = request_factory.post(url)
        force_authenticate(request, user=dev_user)
        response = view(request, pk=dev_venue.pk)

        assert response.status_code == 400
        assert response.data['error'] == 'You already made a checkin in this Venue today.'

    @pytest.mark.django_db
    def test_vote(self, request_factory, dev_user, dev_venue):
        view = VenueViewSet.as_view({'post': 'vote'})
        url = reverse('venues-vote', kwargs={'pk': dev_venue.pk})
        data = {'liked': True}

        request = request_factory.post(url, data=data)
        force_authenticate(request, user=dev_user)
        response = view(request, pk=dev_venue.pk)

        assert response.status_code == 201
        assert response.data['success'] is True
