import pytest


@pytest.fixture(scope='session')
def request_factory():
    from rest_framework.test import APIRequestFactory
    return APIRequestFactory()


@pytest.fixture
def dev_user():
    from core.models import CoreUser
    return CoreUser.objects.create(username='dev', email='dev_user@gmai.com', password='dev')


@pytest.fixture(scope='session')
def model_mommy():
    from model_mommy import mommy
    return mommy


@pytest.fixture(scope='session')
def mock():
    import mock
    return mock
