import datetime
import os

import cbs


class BaseSettings(cbs.BaseSettings):
    PROJECT_NAME = 'venues'
    SECRET_KEY = 'x8jpi@a67(-qseg^yh$*ydx^ht85t--fgc=fw-io2no9&o^r-f'
    AUTH_USER_MODEL = 'core.CoreUser'

    GOOGLE_MAPS_API_URL = 'http://maps.googleapis.com/maps/api'

    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': (
            'rest_framework.permissions.IsAuthenticated',
        ),
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.SessionAuthentication',
            'rest_framework.authentication.BasicAuthentication',
            'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        ),
    }

    GEO_DAFAULT_SRIM = 4326
    DEFAULT_DISTANCE_LIST = 10  # search by ten miles by defualt

    @property
    def INSTALLED_APPS(self):
        contrib = super(BaseSettings, self).INSTALLED_APPS

        third_party = (
            'rest_framework',
            'localflavor',
        )

        local = ('core',)

        return contrib + third_party + local

    @property
    def JWT_AUTH(self):
        return {
            'JWT_ENCODE_HANDLER':
            'rest_framework_jwt.utils.jwt_encode_handler',

            'JWT_DECODE_HANDLER':
            'rest_framework_jwt.utils.jwt_decode_handler',

            'JWT_PAYLOAD_HANDLER':
            'rest_framework_jwt.utils.jwt_payload_handler',

            'JWT_PAYLOAD_GET_USER_ID_HANDLER':
            'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

            'JWT_PAYLOAD_GET_USERNAME_HANDLER':
            'rest_framework_jwt.utils.jwt_get_username_from_payload_handler',

            'JWT_RESPONSE_PAYLOAD_HANDLER':
            'rest_framework_jwt.utils.jwt_response_payload_handler',

            'JWT_SECRET_KEY': self.SECRET_KEY,
            'JWT_ALGORITHM': 'HS256',
            'JWT_VERIFY': True,
            'JWT_VERIFY_EXPIRATION': True,
            'JWT_LEEWAY': 0,
            'JWT_EXPIRATION_DELTA': datetime.timedelta(days=10),
            'JWT_AUDIENCE': None,
            'JWT_ISSUER': None,

            'JWT_ALLOW_REFRESH': False,
            'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

            'JWT_AUTH_HEADER_PREFIX': 'JWT',
        }


class LocalSettings(BaseSettings):
    DEBUG = True

    @property
    def DATABASES(self):
        return {
            'default': {
                'ENGINE': 'django.contrib.gis.db.backends.postgis',
                'NAME': self.PROJECT_NAME,
            }
        }

    @property
    def INSTALLED_APPS(self):
        return super(LocalSettings, self).INSTALLED_APPS + (
            'django_extensions',
        )


class ProductionSettings(BaseSettings):
    DEBUG = False


# Apply the settings
cbs.apply(
    '{}Settings'.format(os.environ.get('DJANGO_MODE', 'Local').title()),
    globals()
)
